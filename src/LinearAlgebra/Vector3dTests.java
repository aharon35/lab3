//Aharon Moryoussef
//1732787
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	void testVector3d() {
		Vector3d test = new Vector3d(1,1,2);
		assertEquals("X: " + 1.0 + " Y: " + 1.0 + " Z: " + 2.0 , test.toString());
		System.out.println(test.toString());
		
		Vector3d test2 = new Vector3d(9,12,10);
		assertEquals("X: " + 9.0 + " Y: " + 12.0 + " Z: " + 10.0 , test2.toString());
		System.out.println(test2.toString());
		
	}

	@Test
	void testGetX() {
		Vector3d test = new Vector3d(98,79,2);
		assertEquals(98,test.getX());
		
	}

	@Test
	void testGetY() {
		Vector3d test = new Vector3d(5,100,3);
		assertEquals(100,test.getY());
		
	}

	@Test
	void testGetZ() {
		Vector3d test = new Vector3d(1,1,2);
		assertEquals(2,test.getZ());
		
	}

	@Test
	void testMagnitude() {
		Vector3d test = new Vector3d(4,5,6);
		assertEquals(8.774964387392122,test.magnitude());
	
	}

	@Test
	void testDotProduct() {
		Vector3d one = new Vector3d(1,1,2);
		Vector3d two = new Vector3d(2,3,4);
		assertEquals(13,one.dotProduct(one, two));
	}

	@Test
	void testAdd() {
		Vector3d test = new Vector3d(1,1,2);
		Vector3d inputValue = new Vector3d(2,3,4);
		assertEquals("X: " + 3.0 + " Y: " + 4.0 + " Z: " + 6.0 , test.add(inputValue).toString());
		
		
	}

}
