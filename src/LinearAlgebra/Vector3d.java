//Aharon Moryoussef
//1732787
package LinearAlgebra;

public class Vector3d {
private double x;
private double y;
private double z;

public Vector3d(double x , double y, double z) {
	this.x=x;
	this.y=y;
	this.z=z;
}	
public double getX() {
	return this.x;
}
public double getY() {
	return this.y;
}
public double getZ() {
	return this.z;
}

public double magnitude() {
	return Math.sqrt(
			(this.x * this.x) + 
			(this.y * this.y) + 
			(this.z * this.z));
}

public double dotProduct(Vector3d one, Vector3d two) {
	double dotProduct = (one.getX() * two.getX()) + 
						(one.getY() * two.getY()) +
						(one.getZ() *two.getZ());
	return dotProduct;
}
public Vector3d add(Vector3d newOne) {
	Vector3d addedUp = new Vector3d(this.getX() + newOne.getX(),
									this.getY() + newOne.getY(), 
									this.getZ() + newOne.getZ());
	return addedUp;
							
	
}
public String toString() {
	return "X: " + this.x + " Y: " + this.y + " Z: " + this.z; 
}

}



